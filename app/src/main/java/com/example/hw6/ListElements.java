package com.example.hw6;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;

public class ListElements implements Serializable {
    LinkedList<Item> items;
    int size;
    public ListElements(LinkedList<Item> items) {
        this.items = items;
    }

    public ListElements() {

    }


    public LinkedList<Item> getListItems() {
        return  this.items;
    }

    public void addItem(Item item){
        items.addLast(item);
    }
}
