package com.example.hw6;

import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class AddItemActivity extends AppCompatActivity {

    Item item;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        Button btn = findViewById(R.id.buttonAddItem);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageView mainImageView = findViewById(R.id.imageViewAddItem);
                TextView title = findViewById(R.id.titleAddItem);
                TextView description = findViewById(R.id.descAddItem);

                item = new Item(title.getText().toString(), R.drawable.food2, description.getText().toString());

                Intent intent = new Intent(AddItemActivity.this, MainActivity.class);
                intent.putExtra("itemAdded", item);
                startActivity(intent);
            }
        });

    }

}