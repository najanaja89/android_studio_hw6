package com.example.hw6;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.LinkedList;

public class MainActivity extends AppCompatActivity {
    //Если не статик, то при каждом возварщении к Активити, он пересоздает заново Лист с элементами
    //Поэтому добавляется только один элемент, потому что старый перезатирается
    static LinkedList<Item> items = new LinkedList<Item>();
    static int basket=0;
    ListElements listElements = new ListElements(items);
    @Override
    protected void onCreate(Bundle savedInstanceState) {

//        items.add(new Item("Eda1", R.drawable.food1, "Random Description"));
//        items.add(new Item("Eda2", R.drawable.food2, "Random Description"));
//        items.add(new Item("Eda3", R.drawable.food3, "Random Description"));
//        items.add(new Item("Eda4", R.drawable.food5, "Random Description"));
//        items.add(new Item("Eda5", R.drawable.food4, "Random Description"));
//        items.add(new Item("Eda6", R.drawable.food6, "Random Description"));
//        items.add(new Item("Eda7", R.drawable.foode7, "Random Description"));

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = findViewById(R.id.btnMain);
        Button btnBasket = findViewById(R.id.btnBasket);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        MyAdapter myAdapter = new MyAdapter(listElements.getListItems());
        myAdapter.notifyDataSetChanged();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(myAdapter);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddItemActivity.class);
                startActivity(intent);
            }
        });

        btnBasket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(findViewById(R.id.mainActivity), "Yes No", Snackbar.LENGTH_LONG)
                        .setAction("Basket Count" + Integer.toString(basket), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Toast.makeText(MainActivity.this, Integer.toString(basket), Toast.LENGTH_LONG);
                            }
                        }).show();
            }
        });


        myAdapter.setOnLongClickListener(new MyAdapter.MyOnLongClickListener() {
            @Override
            public void onLongClick(Item item) {
                btnBasket.setText(Integer.toString(basket));
                basket++;
            }
        });


//        myAdapter.setOnTouchListener(new MyAdapter.MyOnTouchListener() {
//            @Override
//            public void onTouch(Item item) {
//                btnBasket.setText("Clicked");
//            }
//        });


        myAdapter.setListener(new MyAdapter.MyOnClickListener() {
            @Override
            public void onClick(Item item) {
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                intent.putExtra("item", item);
                startActivity(intent);
            }
        });

        if (getIntent().hasExtra("itemAdded")) {
            Item item = (Item) getIntent().getSerializableExtra("itemAdded");
            listElements.addItem(item);
            myAdapter.notifyDataSetChanged();
            Log.d("MyAdapter1", "onCreate: Changed" + listElements.getListItems().size());
        }
    }
}