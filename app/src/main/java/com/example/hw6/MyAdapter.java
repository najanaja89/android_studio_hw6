package com.example.hw6;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>  {

    private LinkedList<Item> data;
    private MyOnClickListener onClickListener;
    private MyOnTouchListener onTouchListener;
    private MyOnLongClickListener onLongClickListener;

    public MyAdapter(LinkedList<Item> data){
        this.data = data;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.imageView.setImageResource(data.get(position).resourceId);
        holder.textView.setText(data.get(position).text);
        holder.itemView.setLongClickable(true);



        if(onClickListener !=null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onClick(data.get(position));
                }
            });
        }
        if(onLongClickListener !=null) {
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    onLongClickListener.onLongClick(data.get(position));
                    return true;
                }
            });
        }
        if(onTouchListener !=null){
            holder.itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        onTouchListener.onTouch(data.get(position));

                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        onTouchListener.onTouch(data.get(position));

                    }
                    return true;
                }
            });
        }
    }



    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface MyOnClickListener {
        void onClick(Item item);
    }

    public interface MyOnTouchListener {
        void onTouch(Item item);
    }

    public interface MyOnLongClickListener {
        void onLongClick(Item item);
    }

    void setListener(MyOnClickListener onClickListener){
        this.onClickListener = onClickListener;
    }
    void setOnTouchListener(MyOnTouchListener onTouchListener) {this.onTouchListener = onTouchListener;}
    void setOnLongClickListener(MyOnLongClickListener onLongClickListener) {this.onLongClickListener = onLongClickListener;}

    static class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;

        public MyViewHolder(@NonNull View v) {
            super(v);
            imageView = v.findViewById(R.id.imageView);
            textView = v.findViewById(R.id.textView);

        }
    }
}
