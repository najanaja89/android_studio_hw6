package com.example.hw6;

import java.io.Serializable;

public class Item implements Serializable {
    public String text;
    public String description;
    public int resourceId;

    public Item(String text, int resourceId, String description) {
        this.text = text;
        this.resourceId = resourceId;
        this.description = description;
    }
}
