package com.example.hw6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class DetailsActivity extends AppCompatActivity {
    ImageView mainImageView;
    TextView title, description;
    String data1, data2;
    int myImage;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        mainImageView = findViewById(R.id.imageViewDetails);
        title = findViewById(R.id.titleDetails);
        description = findViewById(R.id.descDetails);
        Item item = getData();
        setData(item);
    }

    private Item getData(){
        if(getIntent().hasExtra("item"))
        {
            Item item= (Item) getIntent().getSerializableExtra("item");
            return item;
        }
        else {
            Toast.makeText(this, "no data", Toast.LENGTH_SHORT).show();
            return null;
        }
    }

    private void setData(Item item){
        title.setText(item.text);
        description.setText(item.description);
        mainImageView.setImageResource(item.resourceId);

    }

}